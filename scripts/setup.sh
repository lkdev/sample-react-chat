#!/usr/bin/env bash

docker-compose down -v
docker-compose run chat-server npm install
docker-compose run socket-server  npm install
