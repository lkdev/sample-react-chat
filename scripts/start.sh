#!/usr/bin/env bash

docker-compose stop chat-server socket-server
docker-compose up -d
./scripts/logs.sh
