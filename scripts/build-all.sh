#!/usr/bin/env bash

WORKDIR=$PWD
TAG=${1:-latest}

echo 'Building the client app'
cd $WORKDIR/client
./scripts/build.sh $TAG
echo 'Done building the client app'

echo 'Building the chat-server app'
cd $WORKDIR/chat-server
./scripts/build.sh $TAG
echo 'Done building the server app'

echo 'Building the socket-server app'
cd $WORKDIR/socket-server
./scripts/build.sh $TAG
echo 'Done building the socket-server app'