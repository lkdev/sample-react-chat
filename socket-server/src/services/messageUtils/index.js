import { isString, isPlainObject } from 'lodash';

export function getDataFromMessage(message) {
  try {
    const { type, data } = JSON.parse(message);
    return { type, data };
  } catch (e) {
    return { type: 'string', data: message };
  }
}

export function getMessageFromData(data) {
  if (isPlainObject(data)) {
    return JSON.stringify(data);
  }

  if (isString(data)) {
    return data;
  }

  if (data.toString) {
    return data.toString();
  }

  throw new Error('Unsupported socket data type');
}
