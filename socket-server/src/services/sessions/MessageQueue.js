import client from 'src/api/redisClient';

const PREFIX = 'session::messages::';
const EXPIRY = 600;

export default class MessageQueue {
  constructor(sessionId) {
    this.key = `${PREFIX}${sessionId}`;
  }

  async length() {
    return client.llenAsync(this.key);
  }

  async enqueue(message) {
    await client.rpushAsync(this.key, message);
    await client.expireAsync(this.key, EXPIRY);
  }

  async deque() {
    return client.lpopAsync(this.key);
  }

  async resolve(send) {
    const length = await this.length();
    if (!length) return;

    const records = await client.lrangeAsync(this.key, 0, -1);
    await client.delAsync(this.key);

    records.forEach(r => send(r));
    await this.resolve(send);
  }
}
