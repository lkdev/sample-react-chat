import uuid from 'uuid';

import Session from 'src/services/sessions/Session';
import { getMessageFromData } from 'src/services/messageUtils';
import socketStore from 'src/services/sockets/SocketStore';

function generateId() {
  return uuid.v4();
}

export default class Socket {
  constructor(ws) {
    this.id = generateId();
    this.ws = ws;

    ws.socketId = this.id; // eslint-disable-line no-param-reassign
    ws.socket = this; // eslint-disable-line no-param-reassign
  }

  terminate() {
    console.log(`Socket::terminate - socketId: ${this.id}`);
    this.ws.terminate();
    delete this.ws;
    this.ws = undefined;
    socketStore.remove(this);
  }

  async sendJson(data) {
    const message = getMessageFromData(data);
    await this.send(message);
  }

  async send(message) {
    try {
      this.ws.send(message);
    } catch (e) {
      console.log(e);

      const session = await Session.findById(this.sessionId);
      session.close();
      session.enqueueMessage(message);
    }
  }
}
