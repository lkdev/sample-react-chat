import { createClient } from 'src/api/redisClient';
import { getSocketById } from 'src/services/sockets';

const SEND_CHANNEL = 'websocket-send:';
const SEND_CHANNEL_MATCH = `${SEND_CHANNEL}*`;
const RPC_CHANNEL = 'websocket-rpc';
const subscriber = createClient();

subscriber.on('pmessage', (pattern, channel, message) => {
  const socketId = channel.split(':')[1];
  const socket = getSocketById(socketId);

  if (socket) {
    socket.send(message);
  }
});

subscriber.on('message', (channel, payload) => {
  const { method, socketId, data } = JSON.parse(payload);

  const socket = getSocketById(socketId);
  if (socket) {
    socket[method](data);
  }
});

export default function initSockets() {
  console.log(`Subscribe redis channel: ${SEND_CHANNEL_MATCH}`);
  subscriber.psubscribe(SEND_CHANNEL_MATCH);

  console.log(`Subscribe redis channel: ${RPC_CHANNEL}`);
  subscriber.subscribe(RPC_CHANNEL);
}
