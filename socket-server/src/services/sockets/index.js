import Socket from './Socket';
import socketStore from './SocketStore';

export function createSocket(ws) {
  const socket = new Socket(ws);
  socketStore.add(socket);
  return socket;
}

export function getSocketById(id) {
  return socketStore.getById(id);
}

export { default as initSockets } from './pubSub';
