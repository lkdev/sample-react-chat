import redis from 'redis';
import bluebird from 'bluebird';

import { REDIS_HOST, REDIS_PORT } from 'src/settings';

bluebird.promisifyAll(redis);

export function createClient(port = REDIS_PORT, host = REDIS_HOST) {
  return redis.createClient(port, host);
}

export default createClient();
