import axios from 'axios';

import { CHAT_SERVICE_BASE_URL as BASE_URL } from 'src/settings';

export function forwardSessionMessage(sessionId, type, data) {
  return axios.post(`${BASE_URL}/service/sessions/${sessionId}/${type}`, {
    data,
  });
}

export function deleteSession(sessionId) {
  return axios.delete(`${BASE_URL}/service/sessions/${sessionId}`);
}
