import express from 'express';

import { Server } from 'ws';
import { startsWith } from 'lodash';

import { getDataFromMessage } from 'src/services/messageUtils';
import { createSocket, initSockets } from 'src/services/sockets';
import { WS_PORT, API_PORT } from 'src/settings';

import resumeSession from './routes/wss/resumeSession';
import forwardMessage from './routes/wss/forwardMessage';
import serviceRouter from './routes/service';

const wss = new Server({ port: WS_PORT, path: '/ws' });

function handleMessage(socket, message) {
  if (message === 'ping') {
    socket.send('pong');
  } else if (startsWith(message, 'session-resume::')) {
    const data = message.replace('session-resume::', '');
    resumeSession(socket, data);
  } else {
    try {
      const { type, data } = getDataFromMessage(message);

      if (type === 'session-resume') {
        resumeSession(socket, data);
      } else {
        forwardMessage(socket, type, data);
      }
    } catch (e) {
      console.log(e);
    }
  }
}

wss.on('connection', ws => {
  const socket = createSocket(ws);
  ws.on('message', message => handleMessage(socket, message));
});

const app = express();
app.use(express.json());
app.use('/service/', serviceRouter);

app.listen(API_PORT, () => {
  console.log(`Server listening on port ${API_PORT}!`);
});

initSockets();
