import { Router } from 'express';

import { deleteSession } from 'src/api/chatService';
import Session from 'src/services/sessions/Session';
import { getMessageFromData } from 'src/services/messageUtils';

const router = new Router();

router.post('/sessions/:id/send-message/', async (req, res) => {
  const { id } = req.params;
  const data = getMessageFromData(req.body);

  const session = await Session.findById(id);
  if (session) {
    session.send(data);
  } else {
    deleteSession(id);
  }

  res.send({ status: 'ok' });
});

export default router;
