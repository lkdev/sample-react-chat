import { forwardSessionMessage } from 'src/api/chatService';

export default async function forwardMessage(socket, type, data) {
  await forwardSessionMessage(socket.sessionId, type, data);
}
