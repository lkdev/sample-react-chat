import { Router } from 'express';

import models from 'src/models';
import handleChatLogin from './chat/handleChatLogin';
import handleChatNewMessage from './chat/handleChatNewMessage';

const router = Router();

async function getOrCreateSession(serviceId) {
  const [session] = await models.Session.findOrCreate({ where: { serviceId } });
  return session;
}

router.post('/sessions/:serviceId/chat-login', async (req, res) => {
  const { serviceId } = req.params;
  const session = await getOrCreateSession(serviceId);

  handleChatLogin(session);
  res.send('ok');
});

router.post('/sessions/:serviceId/chat-message', async (req, res) => {
  const { serviceId } = req.params;
  const { data } = req.body;
  const session = await getOrCreateSession(serviceId);

  handleChatNewMessage(session, data);
  res.send('ok');
});

router.delete('/sessions/:serviceId', async (req, res) => {
  const { serviceId } = req.params;
  const session = await models.Session.findOne({ where: { serviceId } });
  if (session) {
    await session.destroy();
  }
  res.send('ok');
});

export default router;
