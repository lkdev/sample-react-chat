import sillyname from 'sillyname';

import { sendChatHistory, joinChatRoom } from 'src/services/chats';

import models from 'src/models';

export default async function handleChatLogin(session) {
  let user = await session.getUser();
  if (!user) {
    user = await models.User.create({
      name: sillyname(),
    });
    await session.setUser(user);
  }

  await joinChatRoom(user);
  session.send({
    type: 'chat-login',
    data: user.get({ plain: true }),
  });

  sendChatHistory(session);
}
