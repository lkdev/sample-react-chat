import { addChat, broadcastChat } from 'src/services/chats';

export default async function handleChatNewMessage(session, text) {
  const user = await session.getUser();

  const chat = await addChat(user, text);
  broadcastChat(chat);
}
