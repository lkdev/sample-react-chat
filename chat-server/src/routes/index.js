import apiRouter from './api';
import serviceRouter from './service';

function registerRoutes(app) {
  app.use('/api', apiRouter);
  app.use('/service', serviceRouter);
}

module.exports = registerRoutes;
