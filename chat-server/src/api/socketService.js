import axios from 'axios';

import { SOCKET_SERVICE_BASE_URL as BASE_URL } from 'src/settings';

export default function sendMessage(sessionId, data) {
  axios.post(`${BASE_URL}/service/sessions/${sessionId}/send-message`, data);
}
