import express from 'express';

import { API_PORT } from 'src/settings';
import { initModels } from 'src/models';

const app = express();
app.use(express.json());

const registerRoutes = require('./routes');

registerRoutes(app);

async function main() {
  await initModels();

  app.listen(API_PORT, () => {
    console.log(`Server listening on port ${API_PORT}!`);
  });
}

main().catch(e => {
  console.log(e);
  console.log();
  console.log('An error has occurred. Exiting.');
  process.exit(1);
});
