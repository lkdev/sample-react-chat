import { reverse } from 'lodash';
import models from 'src/models';

const DEFAULT_ROOM = 'general';

async function getChatRoom() {
  const [chatRoom] = await models.ChatRoom.findOrCreate({
    where: { name: DEFAULT_ROOM },
  });
  return chatRoom;
}

export async function addChat(user, text) {
  const chatRoom = await getChatRoom();

  const chat = await chatRoom.createChat({
    text,
    userId: user.id,
  });
  return chat;
}

export async function broadcastChat(chat) {
  const chatRoom = await getChatRoom();
  chatRoom.broadcast(chat);
}

export async function joinChatRoom(user) {
  const chatRoom = await getChatRoom();
  chatRoom.addUser(user);
  await chatRoom.save();
}

export async function sendChatHistory(session) {
  const chatRoom = await getChatRoom();

  // get the latest 100 messages, but in reverse order
  const chats = await models.Chat.findAll({
    where: { chatRoomId: chatRoom.id },
    order: [['id', 'DESC']],
    limit: 100,
    include: [
      {
        model: models.User,
        required: false,
        as: 'user',
      },
    ],
  });

  const data = {
    type: 'chat-load',
    data: reverse(chats.map(c => c.get())),
  };

  session.send(data);
}
