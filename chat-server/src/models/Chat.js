export default (sequelize, DataTypes) => {
  const options = {
    comment: 'chat',
  };

  const attributes = {
    chatRoomId: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
    },
    userId: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
    },
    text: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
  };

  const Chat = sequelize.define('Chat', attributes, options);

  Chat.associate = (models, opts) => {
    Chat.belongsTo(models.ChatRoom, {
      ...opts,
      foreignKey: 'chatRoomId',
      as: 'chatRoom',
    });
    Chat.belongsTo(models.User, {
      ...opts,
      foreignKey: 'userId',
      as: 'user',
    });
  };

  return Chat;
};
