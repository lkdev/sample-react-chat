export default (sequelize, DataTypes) => {
  const options = {
    comment: 'user',
  };

  const attributes = {
    // loginId: {
    //   type: DataTypes.STRING(40),
    //   allowNull: false,
    // },
    // password: {
    //   type: DataTypes.STRING(255),
    //   allowNull: false,
    // },
    // loginToken: {
    //   type: DataTypes.STRING(255),
    // },
    name: {
      type: DataTypes.STRING(100),
    },
  };

  const User = sequelize.define('User', attributes, options);

  User.associate = (models, opts) => {
    User.hasMany(models.Session, {
      ...opts,
      foreignKey: 'userId',
    });
    User.hasMany(models.Chat, {
      ...opts,
      foreignKey: 'userId',
    });
    User.belongsToMany(models.ChatRoom, {
      through: models.ChatRoomUser,
      foreignKey: 'userId',
      otherKey: 'chatRoomId',
    });
  };

  return User;
};
