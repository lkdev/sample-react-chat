export default (sequelize, DataTypes) => {
  const options = {
    comment: 'chatroom-user',
  };

  const attributes = {
    chatRoomId: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
    },
    userId: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
    },
  };

  const ChatRoomUser = sequelize.define('ChatRoomUser', attributes, options);

  ChatRoomUser.associate = (models, opts) => {
    ChatRoomUser.belongsTo(models.ChatRoom, {
      ...opts,
      foreignKey: 'chatRoomId',
    });
    ChatRoomUser.belongsTo(models.User, {
      ...opts,
      foreignKey: 'userId',
    });
  };

  return ChatRoomUser;
};
