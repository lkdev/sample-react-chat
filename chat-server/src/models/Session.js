import { assign } from 'lodash';

import sendMessage from 'src/api/socketService';

export default (sequelize, DataTypes) => {
  const options = {
    comment: 'user',
  };

  const attributes = {
    userId: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
    },
    serviceId: {
      type: DataTypes.STRING(40),
      allowNull: false,
    },
  };

  const instanceMethods = {
    send(message) {
      console.log(`sending message - serviceId: ${this.serviceId}`);
      sendMessage(this.serviceId, message);
    },
  };

  const Session = sequelize.define('Session', attributes, options);

  Session.associate = (models, opts) => {
    Session.belongsTo(models.User, opts);
  };

  assign(Session.prototype, instanceMethods);

  return Session;
};
