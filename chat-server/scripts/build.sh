#!/usr/bin/env bash
source .env
TAG=${1:-latest}

echo $IMAGE_REGISTRY:$TAG

# Build the server app, and copy to docker folder
npm run build
cp -r build/. docker/build
cp package.json docker/build/package.json
cp package-lock.json docker/build/package-lock.json

docker build -t $IMAGE_REGISTRY:$TAG docker
docker push $IMAGE_REGISTRY:$TAG

# Clean up the build artifacts
rm -rf build
rm -rf docker/build
