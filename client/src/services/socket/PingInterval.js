import autobind from 'autobind';

const PING_INTERVAL = 30000; // miliseconds
const PING_INTERVAL_IDLE = 60000; // miliseconds

function getPingInterval() {
  return document.hidden ? PING_INTERVAL_IDLE : PING_INTERVAL;
}

export default class PingInterval {
  constructor(target) {
    this.target = target;
  }

  stop() {
    if (!this.timer) {
      return;
    }

    clearTimeout(this.timer);
    delete this.timer;
    this.timer = undefined;
  }

  start() {
    this.stop();
    this.handleInterval();
  }

  @autobind
  handleInterval() {
    this.target();
    this.timer = setTimeout(this.handleInterval, getPingInterval());
  }
}
