export default class MessageQueue {
  queue = [];

  enqueue(message) {
    this.queue.push(message);
  }

  deque() {
    const [first, ...queue] = this.queue;
    this.queue = queue;
    return first;
  }

  resolve(send) {
    while (this.queue.length) {
      const message = this.deque();
      setTimeout(() => send(message), 0);
    }
  }
}
