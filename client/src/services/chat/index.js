import SessionSocket from 'src/services/socket';
import { WS_URL } from 'src/settings';

export default class ChatConnection {
  constructor({ loadChats, addChat, setCurrentUser }) {
    this.loadChats = loadChats;
    this.addChat = addChat;
    this.setCurrentUser = setCurrentUser;

    this.connect();
  }

  connect() {
    this.close();
    this.socket = new SessionSocket(WS_URL);

    this.socket.onmessage = this.handleOnMessage;
    this.socket.send({ type: 'chat-login' });
  }

  close() {
    if (this.socket) {
      this.socket.close();
      delete this.socket;
    }
  }

  handleOnMessage = msgObj => {
    switch (msgObj.type) {
      case 'chat-load':
        this.loadChats(msgObj.data);
        break;
      case 'chat-message':
        this.addChat(msgObj.data);
        break;
      case 'chat-login':
        this.setCurrentUser(msgObj.data);
        break;
      default:
        break;
    }
  };

  send = obj => this.socket.send(obj);

  sendJson = obj => this.socket.send(obj);
}
