import React, { Component } from 'react';

import ChatMessage from '../ChatMessage';
import StickyBottomScroller from './StickyBottomScroller';

export default class ChatMessages extends Component {
  renderChat = chat => {
    const { currentUser } = this.props;
    const { text, createdAt, user: { id, name } = {} } = chat;

    return (
      <ChatMessage
        currentUser={currentUser}
        key={chat.id}
        userId={id}
        userName={name}
        text={text}
        createdAt={createdAt}
      />
    );
  };

  handleDivRev = ref => {
    this.divRef = ref;
  };

  scrollToBottom = () => {
    if (this.divRef && this.divRef.scrollToBottom) {
      this.divRef.scrollToBottom();
    }
  };

  render() {
    const { chats } = this.props;

    return (
      <StickyBottomScroller ref={this.handleDivRev}>
        {chats && chats.length ? (
          chats.map(this.renderChat)
        ) : (
          <p>No messages for now</p>
        )}
      </StickyBottomScroller>
    );
  }
}
