import React, { Component } from 'react';
import ReactResizeDetector from 'react-resize-detector';

import styles from './styles.module.css';

export default class StickyBottomScroller extends Component {
  stickyBottom = true;

  componentDidMount() {
    setTimeout(this.scrollToBottom, 100);
  }

  componentDidUpdate(prevProps) {
    const {
      children: { length: prevLength },
    } = prevProps;
    const {
      children: { length: currentLength },
    } = this.props;

    if (currentLength > prevLength && this.stickyBottom) {
      this.scrollToBottom();
    } else if (!prevLength && currentLength) {
      this.scrollToBottom();
    }
  }

  scrollToBottom = () => {
    const { scrollHeight, clientHeight } = this.divRef;
    const offset = scrollHeight - clientHeight;

    this.divRef.scrollTop = offset;
  };

  handleOnScroll = () => {
    const { scrollHeight, scrollTop, clientHeight } = this.divRef;
    const offset = scrollHeight - clientHeight;

    this.stickyBottom = scrollTop === offset;
  };

  handleOnResize = () => {
    this.scrollToBottom();
  };

  handleDivRev = ref => {
    this.divRef = ref;
  };

  render() {
    const { children } = this.props;
    return (
      <div
        className={styles.container}
        onScroll={this.handleOnScroll}
        ref={this.handleDivRev}
      >
        <ReactResizeDetector handleHeight onResize={this.handleOnResize} />
        {children}
      </div>
    );
  }
}
