import React, { Component } from 'react';

import ChatMessages from '../ChatMessages';
import ChatInput from '../ChatInput';

import styles from './styles.module.css';

export default class ChatBox extends Component {
  handleDivRev = ref => {
    this.divRef = ref;
  };

  handleOnSubmit = () => {
    const { onSend } = this.props;
    onSend();

    if (this.divRef && this.divRef.scrollToBottom) {
      this.divRef.scrollToBottom();
    }
  };

  render() {
    const { chats, onChangeText, text, currentUser } = this.props;

    return (
      <div className={styles.container}>
        <div className={styles.window}>
          <ChatMessages
            chats={chats}
            currentUser={currentUser}
            ref={this.handleDivRev}
          />
          <ChatInput
            onChange={onChangeText}
            onSubmit={this.handleOnSubmit}
            text={text}
          />
        </div>
      </div>
    );
  }
}
