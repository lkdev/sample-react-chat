import React, { PureComponent } from 'react';
import classNames from 'classnames';

import styles from './styles.module.css';

export default class ChatMessage extends PureComponent {
  static defaultProps = {
    userId: null,
    userName: 'unknown',
    text: '',
    createdAt: null,
  };

  getName() {
    const { userId, userName } = this.props;
    return userName || userId || '<name/id not present>';
  }

  getIsCurrentUser() {
    const {
      currentUser: { id: currentUserId },
      userId,
    } = this.props;

    return currentUserId === userId;
  }

  render() {
    const { text } = this.props;
    const name = this.getName();
    const currentUser = this.getIsCurrentUser();

    return (
      <div className={styles.container}>
        <div
          className={classNames(
            styles.textContainer,
            currentUser && styles.textContainerCurrentUser,
          )}
        >
          <p className={classNames(styles.nameText)}>{`${name} :`}</p>
          <p className={classNames(styles.text)}>{text}</p>
        </div>
      </div>
    );
  }
}
