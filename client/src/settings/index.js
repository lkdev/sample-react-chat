// 'development' or 'production'
const NODE_ENV = process.env.NODE_ENV || 'development';

const API_URLS = {
  development: 'ws://localhost:3001/api',
  production: 'wss://chat.apps.bancuh.net/api',
};

const WS_URLS = {
  development: 'ws://localhost:3080/ws',
  production: 'wss://chat.apps.bancuh.net/ws',
};

export const API_URL = API_URLS[NODE_ENV];
export const WS_URL = WS_URLS[NODE_ENV];
